import React, { useState } from "react";
import { useStripe } from "@stripe/react-stripe-js";
import "./CheckOut.css";
import axios from "axios";
const CheckOut = () => {
  const stripe = useStripe();

  const [product, setProduct] = useState({
    name: "Coke",
    description: "Spreed Love With Your COKE 💕",
    images: [
      "https://images.unsplash.com/photo-1554866585-cd94860890b7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=401&q=80",
    ],
    amount: 699,
    currency: "usd",
    quantity: 0,
  });

  const changeQuantity = (n) => {
    setProduct({ ...product, quantity: Math.max(0, product.quantity + n) });
  };

  const checkOut = async (e) => {
    const result = await axios.post("/checkout", {
      line_items: [product],
    });
    console.log("🚀 ~ file: CheckOut.js ~ line 27 ~ checkOut ~ result", result);
    const { error } = stripe.redirectToCheckout({ sessionId: result.data.id });
    if (error) {
      console.log(error);
    }
  };

  return (
    <div className="checkout__root">
      <img
        src={product.images[0]}
        alt="product_image"
        className="product__img"
      />

      <div className="quantity-btns">
        <button className="add" onClick={() => changeQuantity(1)}>
          +
        </button>
        <p className="quantity">{product.quantity}</p>
        <button className="add" onClick={() => changeQuantity(-1)}>
          -
        </button>
      </div>
      <button className="checkout" onClick={checkOut}>
        checkOut
      </button>
    </div>
  );
};

export default CheckOut;
