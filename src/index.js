import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";

// load stripe script and elements => UI components
import { Elements } from "@stripe/react-stripe-js"; // use stripe stripe react element
import { loadStripe } from "@stripe/stripe-js";
import axios from "axios";

axios.defaults.baseURL = process.env.REACT_APP_API_URL;
console.log(
  "🚀 ~ file: index.js ~ line 13 ~ process.env.REACT_APP_API_URL",
  process.env.REACT_APP_API_URL
);

const stripePromise = loadStripe(
  "pk_test_51Iy91ySCZWK7B7j4774kK30GInFsfAXOF33uczBIRtpcO8V9g07Yug2Cw2naSWN2BtIEYHS4hNPCvFhRGFlfk4TB00RSelLMxO"
);

ReactDOM.render(
  <React.StrictMode>
    {/* elements calls load script and passes the published api key and lodes scrips async */}
    <Elements stripe={stripePromise}>
      <App />
    </Elements>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
