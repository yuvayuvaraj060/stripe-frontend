import React from "react";
import "./Payment.css";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { useState } from "react";

import axios from "axios";
const Payment = () => {
  const [succeeded, setSucceeded] = useState(false);
  const [error, setError] = useState(null);
  const [processing, setProcessing] = useState("");
  const [disabled, setDisabled] = useState(true);

  const [amount, setAmount] = useState(null);

  const stripe = useStripe();
  const elements = useElements();

  const createPayment = async (e) => {
    const result = await axios.post("/payment", {
      amount: amount,
    });
    return result;
  };

  const cardStyle = {
    style: {
      base: {
        color: "#32325d",
        fontFamily: "Arial, sans-serif",
        fontSmoothing: "antialiased",
        fontSize: "16px",
        "::placeholder": {
          color: "#32325d",
        },
      },
      invalid: {
        color: "#fa755a",
        iconColor: "#fa755a",
      },
    },
  };

  const handleChange = async (event) => {
    setDisabled(event.empty);
    setError(event.error ? event.error.message : "");
  };

  const handleSubmit = async (ev) => {
    ev.preventDefault();
    createPayment()
      .then(async (result) => {
        const payload = await stripe.confirmCardPayment(
          result.data.client_secret,
          {
            payment_method: {
              card: elements.getElement(CardElement),
            },
          }
        );
        console.log(
          "🚀 ~ file: Payment.js ~ line 66 ~ handleSubmit ~ payload",
          payload
        );
        if (payload.error) {
          setError(`Payment failed ${payload.error.message}`);
          setProcessing(false);
        } else {
          setError(null);
          setProcessing(false);
          setSucceeded(true);
        }
      })
      .catch((error) => {
        console.log(error);
      });
    setProcessing(true);
  };

  return (
    <>
      <div className="amount">
        <span>Card Number = 4000003560000008,</span>
        <span>use exp data "Any grater then to day"</span>
        <span>use any 3 number</span>
        <label htmlFor="amount">
          Enter Amount:
          <input
            type="number"
            name="amount"
            onChange={(e) => {
              setAmount(Math.min(Math.max(e.target.value, 50), 9999999));
            }}
          />
        </label>
      </div>
      <form id="payment-form" onSubmit={handleSubmit}>
        <CardElement
          id="card-element"
          options={cardStyle}
          onChange={handleChange}
        />
        <button disabled={processing || disabled || succeeded} id="submit">
          <span id="button-text">
            {processing ? (
              <div className="spinner" id="spinner"></div>
            ) : (
              "Pay now"
            )}
          </span>
        </button>
        {error && (
          <div className="card-error" role="alert">
            {error}
          </div>
        )}
        <p className={succeeded ? "result-message" : "result-message hidden"}>
          Payment succeeded, see the result in your
          <a href={`https://dashboard.stripe.com/test/payments`}>
            {" "}
            Stripe dashboard.
          </a>{" "}
          Refresh the page to pay again.
        </p>
      </form>
    </>
  );
};

export default Payment;
